const create360Viewer = require('360-image-viewer');
const canvasFit = require('canvas-fit');

// load your image
const image = new Image();
image.crossOrigin = '';
fetch('https://s3-ap-northeast-1.amazonaws.com/360.adventrip.net/1.JPG', { method: 'get' })
	.then(function(response) {
		console.log(response);
		image.src = response.url;
	}).catch(function(err) {
});
const autoSpin = true;
image.onload = () => {
  // when the image is loaded, setup the viewer
  const viewer = create360Viewer({
    image: image
  });

  // attach canvas to body
  document.body.appendChild(viewer.canvas);

  // setup fullscreen canvas sizing
  const fit = canvasFit(viewer.canvas, window, window.devicePixelRatio);
  window.addEventListener('resize', fit, false);
  fit();

  // start the render loop
  viewer.start();

	viewer.on('tick', (dt) => {
		if (autoSpin && !viewer.controls.dragging) {
			viewer.controls.theta -= dt * 0.00005;
		}
	});
};

let inter = setInterval(() => {
	changeImage();
}, 5000);

function changeImage() {
	let num_str = image.src.replace('.JPG', '').replace('https://s3-ap-northeast-1.amazonaws.com/360.adventrip.net/', '');
	let num = Number(num_str);
	if (num >= 6) {
		clearInterval(inter);
		return 0;
	}
	image.src = 'https://s3-ap-northeast-1.amazonaws.com/360.adventrip.net/' + (num + 1) + ".JPG";
}